import java.util.Random;
public class Die{
	private int pips;
	private Random r;
	
	public Die(){
		this.pips=1;
		this.r= new Random();
	}
	
	public int getPips(){
		return this.pips;
	}
	
	public void roll(){
		
		this.pips=r.nextInt(6)+1; 
		
	}
	
	public String toString(int pips){
		return toString(pips);
	}
}