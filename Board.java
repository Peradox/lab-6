public class Board{
	private Die die1;
	private Die die2;
	private boolean[] closedTiles;
	
	public Board(){
		this.die1= new Die();
		this.die2= new Die();
		this.closedTiles= new boolean[12];
	}
	public boolean playATurn(){
		boolean gameOver=false;
		int sum;
		die1.roll();
		die2.roll();
		sum=die1.getPips()+die2.getPips();
		
		System.out.println("die 1="+die1.getPips()+" die 2="+die2.getPips());
		System.out.println("sum:"+sum);
		
		for(int i=0;i<this.closedTiles.length;i++){
			if(this.closedTiles[i]&&sum==i+1){
				System.out.println("This tile is already closed.");
				gameOver=true;
				break;
			}
			else if(sum==i+1){
				System.out.println("Closing tile:"+(i+1));
				this.closedTiles[i]=true;
				gameOver=false;
			}
				
		}
		return gameOver;
	}
	
	public String toString(){
		
		String s="";
		for(int i=0;i<this.closedTiles.length;i++){
			
			if(closedTiles[i]){
				s=s+"X ";
			}
			else{
				int number=i+1;
				s=s+number+" ";
			}
		}
		return s;
	}
}